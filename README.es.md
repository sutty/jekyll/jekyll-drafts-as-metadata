[English](README.en.md)

Este complemento para Jekyll permite marcar los artículos como
borradores en sus metadatos, en lugar de cambiar los archivos de lugar
al directorio `_drafts/`.

Para marcar un artículo como borrador, agregar a los metadatos:

```
---
draft: true
layout: post
---
```

Al generar el sitio sin la opción `--drafts`, Jekyll se saltea los
artículos marcados como borrador.

Agrega este complemento tan temprano como puedas en la lista de
complementos de `_config.yml`:

```
plugins:
- jekyll-drafts-as-metadata
- ...
```
