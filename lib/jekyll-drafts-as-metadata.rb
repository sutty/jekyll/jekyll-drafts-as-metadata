# frozen_string_literal: true

Jekyll::Hooks.register :site, :post_read, priority: :high do |site|
  next if site.show_drafts

  site.collections.each do |name, col|
    Jekyll.logger.info "Removing drafts from #{name}"
    col.docs.reject! do |doc|
      next unless doc.data.dig('draft')

      Jekyll.logger.info "* #{doc.data.dig('title')}"

      true
    end
  end
end
