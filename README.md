[Castellano](README.es.md)

This Jekyll plugin allows to add draft status as article metadata,
instead of moving files along directories.

To mark an article as draft, just add it to the front matter:

```
---
draft: true
layout: post
---
```

And if you build the site without the `--drafts` flags, Jekyll will skip
them.

Add it as soon as possible on the plugins array in `_config.yml`:

```
plugins:
- jekyll-drafts-as-metadata
- ...
```
