lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |gem|
  gem.name          = 'jekyll-drafts-as-metadata'
  gem.version       = '0.0.5'
  gem.authors       = ['Sutty']
  gem.email         = ['hi@sutty.nl']
  gem.description   = 'Allows to add `draft: true` to front matter to skip them, instead of using `_drafts/`'
  gem.summary       = 'A Jekyll plugin to add draft status as metadata'
  gem.homepage      = 'https://0xacab.org/sutty/jekyll/jekyll-drafts-as-metadata'
  gem.license       = 'GPL-3.0+'

  gem.files         = `git ls-files`.split($INPUT_RECORD_SEPARATOR)
  gem.executables   = gem.files.grep(%r{^bin/}).map { |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ['lib']

  gem.add_runtime_dependency 'jekyll', '>= 3.8', '< 5'
  gem.add_development_dependency('rubocop', '~> 0.68')
  gem.add_development_dependency('rubocop-performance', '~> 0')
end
